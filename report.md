| num    | Stages                 | Start date  |     End Date      | Commit |
| ------ | ---------------------- |-------------|-------------------|--------|
| 1      | Task Clarification     | 06.04.2023  |    08.04.2023     | Clarified details and requirements with the mentor |
| 2      | Analysis               | 09.04.2023  |    12.04.2023     | Study the applied area according to the individual task |
| 3      | Use Cases              | 13.04.2023  |    17.04.2023     | Learned how the program should behave from the user's point of view |
| 4      | Search for Solutions   | 18.04.2023  |    25.04.2023     | Analyzed the methodological material and choose the architecture |
| 4      | Search for Solutions   | 18.04.2023  |    25.04.2023     | Analyzed the methodological material and choose the architecture |
| 5      | Software Development   | 26.04.2023  | under development | Implementing a program with functionalities |
| 6      | Development Completion | not started |    not started    | 1 week |
| 7      | Presentation           | not started |    not started    | 1 week |  
