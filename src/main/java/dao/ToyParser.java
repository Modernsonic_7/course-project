package dao;

import entity.Toy;

import java.util.ArrayList;
import java.util.List;

public class ToyParser {

    public List<Toy> parseStringArr(List<String[]> data) {
        List<Toy> toyList = new ArrayList<>();
        for (int i = 1; i < data.size(); i++) {
            String[] object = data.get(i);
            toyList.add(new Toy(
                    Long.parseLong(object[0]),
                    object[1],
                    object[2],
                    object[3],
                    object[4],
                    object[5],
                    Integer.parseInt(object[6]),
                    Integer.parseInt(object[7]),
                    Double.parseDouble(object[8])
            ));
        }
        return toyList;
    }

}
