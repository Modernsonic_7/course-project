package dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ToyDAOImpl implements ToyDAO{
    private final String PATH = "src/main/resources/toysData.csv";
    @Override
    public List<String[]> getObjectsFromCsv() {
        List<String[]> list = new ArrayList<>();
        FileReader fileReader;
        try {
            fileReader = new FileReader(PATH);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String row = "";
            while ((row = bufferedReader.readLine()) != null) {
                String[] product = row.split(",");
                list.add(product);
            }
            return list;
        } catch (IOException e) {
            System.err.println("File not found or something wrong with url.Please tell developer to change url"+e.getMessage());
        }
        return new ArrayList<>();
    }
}
