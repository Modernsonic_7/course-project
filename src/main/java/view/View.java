package view;

import controller.ToyController;
import controller.ToyControllerImpl;
import dao.ToyDAO;
import dao.ToyDAOImpl;
import dao.ToyParser;
import service.ToyService;
import service.ToyServiceImpl;

import java.util.Scanner;

public class View {
    public static void run() {
        ToyDAO toyDAO = new ToyDAOImpl();
        ToyParser toyParser = new ToyParser();
        ToyService toyService = new ToyServiceImpl(toyDAO, toyParser);
        ToyController toyController = new ToyControllerImpl(toyService);

        System.out.println("""
                Project name: Toy warehouse. Course project. FOP
                Developer: Khumoyun Makhammatov. Khumoyun_Makhammatov@student.itpu.uz
                Creation date: May 2023
                Version: version_1
                """);
        boolean whileBool = true;
        while (true) {
            System.out.println("""
                    Menu commands:
                    LIST
                    SEARCH
                    STOP
                    """);

            Scanner scanner = new Scanner(System.in);
            String menuCommand = scanner.nextLine();

            if (menuCommand.equalsIgnoreCase("list")) {
                System.out.println(toyController.getAll());
            } else if (menuCommand.equalsIgnoreCase("search")) {
                System.out.println("""
                        Toys can be search by this fields:
                        CATEGORY
                        BRAND
                        AGE RANGE
                        PRICE
                        """);

                String inputSearchCommand = scanner.nextLine();
                if (inputSearchCommand.equalsIgnoreCase("category")) {
                    System.out.println(toyController.getAllCategoryNames() + "Please choose:");
                    System.out.println(toyController.getAllByCategoryName(scanner.nextLine()));
                }
                else if (inputSearchCommand.equalsIgnoreCase("brand")) {
                    System.out.println(toyController.getAllBrandNames() + "Please choose:");
                    System.out.println(toyController.getAllByBrandName(scanner.nextLine()));
                }
                else if (inputSearchCommand.equalsIgnoreCase("age range")) {
                    System.out.println("0-2 years\n3-4 years\n5-7 years\n8-10 years\n10-15 years");
                    System.out.println("Enter minimal age :");
                    String minAge = scanner.nextLine();
                    System.out.println("Enter maximal age :");
                    String maxAge = scanner.nextLine();
                    System.out.println(toyController.getAllByAgeRangeFromMinAndMaxAge(minAge, maxAge));
                }
                else if (inputSearchCommand.equalsIgnoreCase("price")) {
                    System.out.println("Enter minimal price :");
                    String  minimalPrice = scanner.nextLine();
                    System.out.println("Enter maximal price :");
                    String  maximalPrice = scanner.nextLine();
                    System.out.println(toyController.getAllByMinAndMaxPrice(minimalPrice, maximalPrice));
                }
                else {
                    System.err.println("Invalid search command entered!!!.Please try again");
                }


            } else if (menuCommand.equalsIgnoreCase("stop")) {
                System.err.println("Application stopped. Thank you for your attention!!!");
                break;
            } else {
                System.err.println("Invalid command entered!!!");
            }
        }

    }
}
