package controller;

import entity.Toy;

import java.util.List;
import java.util.Set;

public interface ToyController {
    List<Toy> getAll();

    Set<String> getAllCategoryNames();

    List<Toy> getAllByCategoryName(String categoryName);

    Set<String> getAllBrandNames();

    List<Toy> getAllByBrandName(String brandName);

    List<Toy> getAllByAgeRangeFromMinAndMaxAge(String minAge, String maxAge);

    List<Toy> getAllByMinAndMaxPrice(String minimalPrice, String  maximalPrice);
}
