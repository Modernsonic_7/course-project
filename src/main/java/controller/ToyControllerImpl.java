package controller;

import entity.Toy;
import service.ToyService;

import java.util.List;
import java.util.Set;

public class ToyControllerImpl implements ToyController {
    private final ToyService toyService;

    public ToyControllerImpl(ToyService toyService) {
        this.toyService = toyService;
    }

    @Override
    public List<Toy> getAll() {
        return toyService.getAll();
    }

    @Override
    public Set<String> getAllCategoryNames() {
        return toyService.getAllCategoryNames();
    }

    @Override
    public List<Toy> getAllByCategoryName(String categoryName) {
        return toyService.getAllByCategoryName(categoryName);
    }

    @Override
    public Set<String> getAllBrandNames() {
        return toyService.getAllBrandNames();
    }

    @Override
    public List<Toy> getAllByBrandName(String brandName) {
        return toyService.getAllByBrandName(brandName);
    }

    @Override
    public List<Toy> getAllByAgeRangeFromMinAndMaxAge(String minAge, String maxAge) {
        return toyService.getAllByAgeRangeFromMinAndMaxAge(minAge,maxAge);
    }

    @Override
    public List<Toy> getAllByMinAndMaxPrice(String minimalPrice, String maximalPrice) {
        return toyService.getAllByMinAndMaxPrice(minimalPrice,maximalPrice);
    }
}
