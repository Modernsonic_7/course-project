package entity;

import java.util.Objects;

public class Toy {
    private Long id;
    private String name;
    private String gender;
    private String category;
    private String brand;
    private String color;
    private int quantity;
    private int  age;
    private Double price;

    public Toy() {
    }

    public Toy(Long id, String name, String gender, String category, String brand, String color, int quantity, int age, Double price) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.category = category;
        this.brand = brand;
        this.color = color;
        this.quantity = quantity;
        this.age = age;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "\nToy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", category='" + category + '\'' +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", quantity=" + quantity +
                ", age=" + age +
                ", price= " + price + "\u20AC"+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Toy toy = (Toy) o;
        return quantity == toy.quantity && age == toy.age && Objects.equals(id, toy.id) && Objects.equals(name, toy.name) && Objects.equals(gender, toy.gender) && Objects.equals(category, toy.category) && Objects.equals(brand, toy.brand) && Objects.equals(color, toy.color) && Objects.equals(price, toy.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, gender, category, brand, color, quantity, age, price);
    }
}
