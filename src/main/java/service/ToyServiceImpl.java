package service;

import dao.ToyDAO;
import dao.ToyParser;
import entity.Toy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ToyServiceImpl implements ToyService {
    private final ToyDAO toyDAO;
    private final ToyParser toyParser;

    public ToyServiceImpl(ToyDAO toyDAO, ToyParser toyParser) {
        this.toyDAO = toyDAO;
        this.toyParser = toyParser;
    }

    public List<Toy> getToysList() {
        return toyParser.parseStringArr(toyDAO.getObjectsFromCsv());
    }

    @Override
    public List<Toy> getAll() {
        if (getToysList().isEmpty()) {
            System.err.println("Any toys not found.Please try again!!!");
            return new ArrayList<>();
        }
        return getToysList();
    }

    @Override
    public Set<String> getAllCategoryNames() {
        Set<String> categories = new HashSet<>();
        for (Toy toy : getToysList()) {
            categories.add(toy.getCategory());
        }
        if (categories.isEmpty()) {
            System.err.println("Any category not found.Please try again!!!");
            return new HashSet<>();
        }
        return categories;
    }

    @Override
    public List<Toy> getAllByCategoryName(String categoryName) {
        List<Toy> toyList = new ArrayList<>();
        for (Toy toy : getToysList()) {
            if (toy.getCategory().equalsIgnoreCase(categoryName)) {
                toyList.add(toy);
            }
        }
        if (toyList.isEmpty()) {
            System.err.println("Nothing found by your input!. Please try again by other category name!!");
            return new ArrayList<>();
        } else {
            return toyList;
        }
    }

    @Override
    public Set<String> getAllBrandNames() {
        Set<String> brands = new HashSet<>();
        for (Toy toy : getToysList()) {
            brands.add(toy.getBrand());
        }
        if (brands.isEmpty()) {
            System.err.println("Any brands not found.Please try again!!!");
            return new HashSet<>();
        }
        return brands;
    }

    @Override
    public List<Toy> getAllByBrandName(String brandName) {
        List<Toy> toyList = new ArrayList<>();
        for (Toy toy : getToysList()) {
            if (toy.getBrand().equalsIgnoreCase(brandName)) {
                toyList.add(toy);
            }
        }
        if (toyList.isEmpty()) {
            System.err.println("Nothing found by your input!. Please try again by other brand name!!");
            return new ArrayList<>();
        } else {
            return toyList;
        }
    }

    @Override
    public List<Toy> getAllByAgeRangeFromMinAndMaxAge(String minAge, String maxAge) {
        List<Toy> toyList = new ArrayList<>();
        int minimalAge = 0;
        int maximalAge = 0;
        try {
            minimalAge = Integer.parseInt(minAge);
            maximalAge = Integer.parseInt(maxAge);
            for (Toy toy : getToysList()) {
                if (toy.getAge() >= minimalAge && toy.getAge() <= maximalAge) {
                    toyList.add(toy);
                }
            }
            if (toyList.isEmpty()) {
                System.err.println("Nothing found by your input!. Please try again by other age range!!");
                return new ArrayList<>();
            }
            return toyList;

        } catch (Exception e) {
            System.err.println("INvalid age range entered.Please try again in correct way!!!");
        }
        return new ArrayList<>();
    }

    @Override
    public List<Toy> getAllByMinAndMaxPrice(String minimalPrice, String maximalPrice) {
        List<Toy> toyList = new ArrayList<>();

        double minimPrice = 0;
        double maximPrice = 0;
        try {
            minimPrice = Double.parseDouble(minimalPrice);
            maximPrice = Double.parseDouble(maximalPrice);
            for (Toy toy : getToysList()) {
                if (toy.getPrice() >= minimPrice && toy.getPrice() <= maximPrice) {
                    toyList.add(toy);
                }
            }
            if (toyList.isEmpty()) {
                System.err.println("Nothing found by your input!. Please try again by other price range!!");
                return new ArrayList<>();
            }
            return toyList;
        } catch (Exception e) {
            System.err.println("Invalid price entered.Please try again!!!");
        }
        return new ArrayList<>();
    }
}
